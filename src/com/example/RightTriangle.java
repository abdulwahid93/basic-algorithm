package com.example;

import java.util.Scanner;

public class RightTriangle {

	public static void main(String[] args) {
		@SuppressWarnings("resource")
		Scanner myObj = new Scanner(System.in);
		Integer inputs = myObj.nextInt();
		triangle(inputs);
	}
	
	public static void triangle(int input) {
		int row,column;
		for(row=0; row<input; row++) {
			for(column=2*(input-row); column>=0; column--) {
				System.out.print(" ");
			}
			for(column=0; column<=row; column++) {
				System.out.print("* ");
			}
			System.out.println();
		}
	}
}
